import requests
from bs4 import BeautifulSoup

URL = 'https://www.amazon.es/Staging-Product-Not-Retail-Sale/dp/B0774LPF8M?pf_rd_p=f65deca5-51be-4e0d-81bc-558127d77030&pd_rd_wg=V80lZ&pf_rd_r=2GYJY2Z598615MRZKTKS&ref_=pd_gw_unk&pd_rd_w=ZWibw&pd_rd_r=49ace9a7-ca9e-4e33-9174-b6b411e56ea8'

headers = {"User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0'}

page = requests.get(URL, headers=headers)

soup = BeautifulSoup(page.content, 'html.parser')

title = soup.find(id="productTitle").get_text()
print(title.strip())